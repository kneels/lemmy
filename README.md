# Lemmy
Monitor websites for changes.

### Prerequisites
Python 3.7 or higher.

Make sure you have the chromedriver installed and added to the PATH.

Linux
```
sudo apt install chromium-chromedriver
```
Windows
```
https://chromedriver.chromium.org/downloads
```
You can also use Firefox with the geckodriver by changing the ```WEB_DRIVER``` option in ```.env```.

Install the requirements.
```
pip3 install -r requirements.txt
```

### Usage
Define the DOM elements to observe in ```pages.yaml```:
```
name: Example page                # Name of the watch
enabled: true                     # Enable this watch
url: https://www.example.com/     # The URL of the website
interval: 30s                     # Check every (s)econds, (m)inutes or (h)ours
elements_to_watch:                # DOM elements to check for changes
  - selector_type: xpath          # xpath, css_selector or css_path
    selector: "/html/head/title"  # The selector
    html: inner                   # Get the element's innerHTML or outerHTML
    wait_for: 10s                 # How long to maximally wait for the element to appear in the DOM (default=5s)
  - selector_type: css_selector
    selector: "body > div:nth-child(1) > h1:nth-child(1)"
    html: outer
---                               # Separate pages with three dashes
```
Set up a notification method in ```.env```:
```
# E-mail settings
NOTIFY_BY_EMAIL=true
TO_NOTIFY_EMAIL=youremailaddress@example.com
SMTP_USERNAME=
SMTP_PASSWORD=
SMTP_SERVER=
SMTP_PORT=587

# Telegram settings
NOTIFY_BY_TELEGRAM=false
TELEGRAM_BOT_TOKEN=
TELEGRAM_CHAT_ID=
```
And then you're ready to go:
```python3 lemmy.py```