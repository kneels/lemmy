from src.monitor import Monitor
from src.database import Database
from src.browser import Browser
from src.config import Config, EmailConfig, TelegramConfig
from src.notifier import Notifier
from src.email_provider import EmailProvider
from src.telegram_provider import TelegramProvider
import yaml
import sys


if __name__ == '__main__':
    db = Database(Config.DATABASE_NAME)
    browser = Browser(Config.WEB_DRIVER, Config.HEADLESS)
    notifier = Notifier()
    if Config.NOTIFY_BY_EMAIL:
        notifier.add_provider(EmailProvider(EmailConfig()))
    if Config.NOTIFY_BY_TELEGRAM:
        notifier.add_provider(TelegramProvider(TelegramConfig()))

    mon = Monitor(db, browser, notifier)

    if len(sys.argv) > 1:
        fn = sys.argv[1]
    else:
        fn = Config.DEFAULT_PAGES_FILE

    with open(fn, 'r+', encoding='utf-8') as f:
        for p in yaml.load_all(f, Loader=yaml.FullLoader):
            if p is None:
                continue
            mon.add_page(p)

    mon.start()
