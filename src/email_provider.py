from src.notifier import NotifierProvider, NotifyMessage
from src.config import EmailConfig
import smtplib
from string import Template

email_template = Template('To:$to\nFrom:$sender\nSubject:$subject\n\n$message\n\n')


class EmailProvider(NotifierProvider):

    def __init__(self, config: EmailConfig):
        self.to = config.TO_NOTIFY_EMAIL
        self.usr = config.SMTP_USERNAME
        self.pwd = config.SMTP_PASSWORD
        self.server = config.SMTP_SERVER
        self.port = config.SMTP_PORT

    def send(self, msg: NotifyMessage):
        smtpserver = smtplib.SMTP(self.server, self.port)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.login(self.usr, self.pwd)
        msg = email_template.substitute(
                    to=self.to,
                    sender=self.usr,
                    subject=msg.header,
                    message=msg.body)

        smtpserver.sendmail(self.usr, self.to, msg.encode('utf8'))

        smtpserver.close()
