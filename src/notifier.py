from threading import Thread
from queue import Queue
from dataclasses import dataclass
import abc


@dataclass
class NotifyMessage:
    header: str
    body: str


class NotifierProvider(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def send(self, msg: NotifyMessage):
        pass


class Notifier(Thread):

    def __init__(self):
        super(Notifier, self).__init__()
        self.queue = Queue()
        self.providers = []
        self.start()

    def __parse_config(self, conf):
        pass

    def run(self):
        while True:
            notification = self.queue.get()
            for p in self.providers:
                p.send(notification)

    def add_provider(self, provider: NotifierProvider):
        self.providers.append(provider)

    def send(self, message):
        self.queue.put(message)
