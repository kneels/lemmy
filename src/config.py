import os
from os.path import join, dirname
from dotenv import load_dotenv
from string import Template
from src.util import str_to_bool


class Config:
    dotenv_path = join(dirname(__file__), '../.env')
    load_dotenv(dotenv_path)

    HEADLESS = str_to_bool(os.environ.get("HEADLESS"))
    DATABASE_NAME = os.environ.get("DB_NAME")
    WEB_DRIVER = os.environ.get("WEB_DRIVER").lower()
    DEFAULT_PAGES_FILE = os.environ.get("DEFAULT_PAGES_FILE")
    NOTIFY_BY_EMAIL = str_to_bool(os.environ.get("NOTIFY_BY_EMAIL"))
    NOTIFY_BY_TELEGRAM = str_to_bool(os.environ.get("NOTIFY_BY_TELEGRAM"))
    NOTIFY_ON_START = str_to_bool(os.environ.get("NOTIFY_ON_START"))
    NOTIFICATION_BODY_TEMPLATE = Template(os.environ.get("NOTIFICATION_BODY_TEMPLATE"))
    NOTIFICATION_HEADER_TEMPLATE = Template(os.environ.get("NOTIFICATION_HEADER_TEMPLATE"))


class EmailConfig(Config):
    TO_NOTIFY_EMAIL = os.environ.get("TO_NOTIFY_EMAIL")
    SMTP_USERNAME = os.environ.get("SMTP_USERNAME")
    SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD")
    SMTP_SERVER = os.environ.get("SMTP_SERVER")
    SMTP_PORT = os.environ.get("SMTP_PORT")


class TelegramConfig(Config):
    TELEGRAM_BOT_TOKEN = os.environ.get("TELEGRAM_BOT_TOKEN")
    TELEGRAM_CHAT_ID = os.environ.get("TELEGRAM_CHAT_ID")

