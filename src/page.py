from src.database import Database
from threading import Thread
from src.util import *
import hashlib
import time
from datetime import datetime
from src.browser import Browser
from typing import Callable
from src.event import Event, EventType


class Page:
    pass # Forward declaration


class Element:

    def __init__(self, page: Page, data: dict):
        self.page = page
        self.selector_type = str_to_selector_type(data['selector_type'])
        self.html = str_to_innerouter_html(data['html'])
        self.selector = data['selector']
        if 'wait_for' in data:
            self.wait_for = str_to_sec(data['wait_for'])
        else:
            self.wait_for = 5
        self.content = None
        self.id = self.__generate_id()

    def __generate_id(self):
        id_str = str(
            str(self.page.id) +
            str(self.selector_type) +
            str(self.html) +
            str(self.selector)).encode('utf-8')
        return hashlib.sha1(id_str).hexdigest()

    def __str__(self):
        return self.id


class Page(Thread):
    def __init__(self, name, enabled, url, interval, elements_to_watch,
                 db: Database, browser: Browser, on_event: Callable[[Page, Event], None]):
        super(Page, self).__init__()
        self.name = name
        self.enabled = enabled
        self.url = url
        self.id = self.__generate_id()
        self.interval = str_to_sec(interval)
        self.elements = []
        self.db = db
        self.setup_elements(elements_to_watch)
        self.browser = browser
        self.on_event = on_event
        self.sec_since_last_check = unix_time_now() - self.db.get_page_last_access(self.id)

    def __generate_id(self):
        id_str = str(
            str(self.url) +
            str(self.name)).encode('utf-8')
        return hashlib.sha1(id_str).hexdigest()

    def setup_elements(self, to_watch):
        for tw in to_watch:
            elem = Element(self, tw)
            elem.content = self.db.get_stored_content(elem.id)
            self.elements.append(elem)

    def get_content_success(self, elem, new_content):
        if elem.content is None:
            self.on_event(self, Event(new_content, EventType.FIRST_RUN))
        else:
            if new_content != elem.content:
                self.on_event(self, Event([new_content, elem.content], EventType.CHANGE_DETECTED))
        elem.content = new_content
        self.db.update_element(elem)
        self.db.update_page(self)

    def get_content_error(self, elem):
        self.on_event(self, Event(elem.selector, EventType.CANT_FIND_ELEMENT))
        self.db.update_page(self)

    def run(self):
        if self.sec_since_last_check < self.interval:
            time.sleep(self.interval - self.sec_since_last_check)
        while True:
            for elem in self.elements:
                self.browser.get_content(self.url, elem, self.get_content_success, self.get_content_error)
            time.sleep(self.interval)
