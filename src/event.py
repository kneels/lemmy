from dataclasses import dataclass
from enum import Enum
from typing import Union


class EventType(Enum):
    CHANGE_DETECTED = 0
    CANT_FIND_ELEMENT = 1
    FIRST_RUN = 2


@dataclass
class Event:
    message: Union[str, list]
    type: EventType
