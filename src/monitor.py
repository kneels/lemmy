from src.database import Database
from src.page import Page
from src.browser import Browser
from src.notifier import Notifier, NotifyMessage
from src.event import Event, EventType
from src.config import Config


class Monitor:
    def __init__(self, db: Database, browser: Browser, notifier: Notifier):
        self.db = db
        self.pages = []
        self.threads = []
        self.browser = browser
        self.notifier = notifier

    def add_page(self, page_data):
        new_page = Page(**page_data, db=self.db, browser=self.browser, on_event=self.on_event)
        self.pages.append(new_page)

    def start(self):
        for p in self.pages:
            if p.enabled:
                p.start()
        if Config.NOTIFY_ON_START:
            self.notifier.send(NotifyMessage('Lemmy info',
                                             'Watching ' + ', '.join([x.name for x in self.pages if x.enabled])))

    def on_event(self, sender: Page, evt: Event):
        if evt.type is EventType.CHANGE_DETECTED:
            body = Config.NOTIFICATION_BODY_TEMPLATE.substitute(
                    name=sender.name,
                    url=sender.url,
                    current=evt.message[0],
                    previous=evt.message[1])
            header = Config.NOTIFICATION_HEADER_TEMPLATE.substitute(name=sender.name)
            self.notifier.send(NotifyMessage(header, body))
        elif evt.type is EventType.CANT_FIND_ELEMENT:
            self.notifier.send(NotifyMessage('Lemmy Error',
                                             f'''Unable to find element '{evt.message}' at {sender.url}'''))

    def stop(self):
        pass
