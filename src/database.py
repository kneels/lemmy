from threading import Thread
from queue import Queue
from src.util import unix_time_now
import sqlite3
import time


class Database(Thread):

    def __init__(self, db):
        super(Database, self).__init__()
        self.db = db
        self.queries = Queue()
        self.start()
        # TODO: clean up records that are not linked to page elements

    def run(self):
        conn = sqlite3.Connection(self.db)
        cursor = conn.cursor()
        self.create_table()
        while True:
            stmt, arg, res = self.queries.get()
            if stmt == '--close--':
                break
            cursor.execute(stmt, arg)
            conn.commit()
            if res:
                res.put(cursor.fetchone())
        conn.close()

    def execute(self, req, arg=None, res=None):
        self.queries.put((req, arg or tuple(), res))

    def select(self, req, arg=None):
        result = Queue()
        self.execute(req, arg, result)
        while True:
            row = result.get()
            yield row

    def select_wait(self, req, arg=None):
        result = Queue()
        self.execute(req, arg, result)
        return result.get()

    def get_stored_content(self, elem_id):
        res = self.select_wait('''SELECT content FROM element WHERE id = ?;''', (elem_id,))
        if res:
            return res[0]
        return None

    def get_page_last_access(self, page_id):
        res = self.select_wait('''SELECT last_access FROM page WHERE id = ?;''', (page_id,))
        if res:
            return int(res[0])
        return 0

    def update_element(self, elem):
        self.execute(
            f'''INSERT OR REPLACE INTO element("id", "content", "page")
             VALUES(?, ?, ?);''', (elem.id, elem.content, elem.page.id))

    def update_page(self, page):
        self.execute(f'''INSERT OR REPLACE INTO page("id", "url", "last_access")
                     VALUES(?, ?, ?);''', (page.id, page.url, unix_time_now()))

    def create_table(self):
        self.execute(f'''CREATE TABLE IF NOT EXISTS `page` (`id` TEXT,
                        `url` TEXT, `last_access` TEXT, PRIMARY KEY(id));''')
        self.execute(f'''CREATE TABLE IF NOT EXISTS `element` (`id` TEXT,
                        `content` TEXT, `page` TEXT, 
                        PRIMARY KEY(id), FOREIGN KEY(`page`) REFERENCES `page.id`);''')

    def close(self):
        self.execute('--close--')
