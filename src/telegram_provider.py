from src.notifier import NotifierProvider, NotifyMessage
from src.config import TelegramConfig
from telegram.ext import Updater, CommandHandler


class TelegramProvider(NotifierProvider):

    def __init__(self, config: TelegramConfig):
        self.config = config

        updater = Updater(config.TELEGRAM_BOT_TOKEN, use_context=True)
        self.dispatcher = updater.dispatcher
        self.dispatcher.add_handler(CommandHandler('ping', TelegramProvider.ping))
        updater.start_polling()

    @staticmethod
    def ping(update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Yep still running")

    def send(self, msg: NotifyMessage):
        self.dispatcher.bot.send_message(chat_id=self.config.TELEGRAM_CHAT_ID, text=msg.body)
