from selenium.webdriver.common.by import By
import time


def str_to_sec(time_str) -> int:
    """
    Converts a string such as '1.5h' or '5m' into whole seconds (5400 and 300 respectively).
    If no/wrong time unit character is provided, then seconds are assumed.
    """
    if type(time_str) == int:
        return time_str
    units = {'s': 1, 'm': 60, 'h': 3600, 'd': 86400}
    unit = time_str[-1].lower()
    if unit in units.keys():
        return int(float(time_str[:-1]) * units[unit])
    else:
        return int(time_str)


def str_to_selector_type(sel_str: str) -> str:
    try:
        return By.__dict__[sel_str.upper()]
    except KeyError:
        return By.CSS_SELECTOR


def str_to_innerouter_html(inner_outer_str: str) -> str:
    if 'inner' in inner_outer_str.lower():
        return 'innerHTML'
    return 'outerHTML'


def str_to_bool(bool_str: str) -> bool:
    return bool_str.lower() == 'true'


def unix_time_now():
    return int(time.time())
