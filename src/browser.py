from threading import Thread
from queue import Queue
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC


class Browser(Thread):
    def __init__(self, web_driver, headless=True):
        super(Browser, self).__init__()
        self.queries = Queue()
        self.headless = headless
        self.web_driver = web_driver
        self.start()

    def run(self):
        if self.web_driver == 'firefox':
            ff_opt = FirefoxOptions()
            ff_opt.headless = self.headless
            driver = webdriver.Firefox(options=ff_opt)
        else:
            chrome_opt = ChromeOptions()
            chrome_opt.headless = self.headless
            driver = webdriver.Chrome(options=chrome_opt)
        while True:
            url, elem, cb_success, cb_error = self.queries.get()
            if url != driver.current_url:
                driver.get(url)
            try:
                web_elem = WebDriverWait(driver, elem.wait_for).until(
                    EC.presence_of_element_located((elem.selector_type, elem.selector)))
                cb_success(elem, web_elem.get_attribute(elem.html))
            except TimeoutException:
                cb_error(elem)

    def get_content(self, url, elem, callback_success, callback_error):
        self.queries.put((url, elem, callback_success, callback_error))
